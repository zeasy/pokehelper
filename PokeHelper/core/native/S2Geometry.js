/**
 * @providesModule S2Geometry
 * Created by easy on 16/7/24.
 */

'use strict'
let NativeS2Geometry = require('NativeModules').NativeS2Geometry;

class S2Geometry {
	static getMapObjectsCellId(latitude,longitude) {
		return NativeS2Geometry.getCellId({latitude,longitude,level:15});
	}
}

module.exports = S2Geometry;