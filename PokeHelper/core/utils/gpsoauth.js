/**
 *
 * @providesModule GoogleOauth
 * Created by easy on 16/7/23.
 */

'use strict'

//var https = require('https');
var querystring = require('querystring');
var url = require('url');
var CryptoJS = require("crypto-js");


var AUTH_URL = 'https://android.clients.google.com/auth';

var USER_AGENT = 'Dalvik/2.1.0 (Linux; U; Android 5.1.1; Andromax I56D2G Build/LMY47V';

var oauthUtil = {};
oauthUtil.parseKeyValues = function (body) {
	var obj = {};
	body.split("\n").forEach(function (line) {
		var pos = line.indexOf("=");
		if (pos > 0) obj[line.substr(0, pos)] = line.substr(pos + 1);
	});
	return obj;
};
oauthUtil.Base64 = {
	_map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_",
	stringify: CryptoJS.enc.Base64.stringify,
	parse: CryptoJS.enc.Base64.parse
};
oauthUtil.salt = function (len) {
	return Array.apply(0, Array(len)).map(function () {
		return (function (charset) {
			return charset.charAt(Math.floor(Math.random() * charset.length));
		}('abcdefghijklmnopqrstuvwxyz0123456789'));
	}).join('');
};

var GoogleOauth = function () {
};


GoogleOauth.prototype.request = function (options, callback) {
	var opt = {};
	opt.headers = {};
	opt.method = options.method || "GET";
	if (typeof options.options === "object") {
		Object.keys(options.options).forEach(function (k) {
			opt[k] = options.options[k];
		});
	}
	if (typeof this._token !== "undefined") opt.headers.Authorization = "GoogleLogin auth=" + this._token;
	opt.headers['User-Agent'] = USER_AGENT;
	opt.headers["Content-type"] = options.contentType || "application/x-www-form-urlencoded";
	opt.body = options.data;
	fetch(options.url,opt).then(async response => {
		var err;
		var body = '';
		if (response.status >= 400) {

			err = new Error(response.status + " error from server");
			err.statusCode = response.status;
			err.response = response;
		} else {
			var contentType = (typeof response.headers.map["content-type"] !== "string") ? null : response.headers.map["content-type"].split(";", 1)[0].toLowerCase();

			if (contentType == 'application/json') {
				body = await response.json();
			} else {
				body = await response.text();
			}
		}
		if (typeof callback === "function") callback(err, body, response);
	}, error => {
		if (typeof callback === "function") callback(error,'');
	});
};


GoogleOauth.prototype.oauth = function (email, master_token, android_id, service, app, client_sig, callback) {

	var data = {
		accountType: "HOSTED",
		Email: email,
		EncryptedPasswd: master_token,
		has_permission: 1,
		service: service,
		source: "android",
		androidId: android_id,
		app: app,
		client_sig: client_sig,
		device_country: "us",
		operatorCountry: "us",
		lang: "en",
		sdk_version: "17"
	};

	this.request({
		method: "POST",
		url: AUTH_URL,
		contentType: "application/x-www-form-urlencoded",
		data: querystring.stringify(data),
		headers: {
		}
	}, function (err, data) {
		callback(err, err ? null : oauthUtil.parseKeyValues(data));
	});
};
GoogleOauth.prototype.login = function (email, password, android_id, callback) {
	var data = {
		accountType: "HOSTED",
		Email: email.trim(),
		has_permission: "1",
		add_account: "1",
		Passwd: password,
		service: "ac2dm",
		source: "android",
		androidId: android_id,
		device_country: "us",
		operatorCountry: "us",
		lang: "en",
		sdk_version: "17"
	};
	this.request({
		method: "POST",
		url: AUTH_URL,
		contentType: "application/x-www-form-urlencoded",
		data: querystring.stringify(data)
	}, function (err, data) {
		var response = oauthUtil.parseKeyValues(data);
		callback(err, err ? null : {androidId: android_id, masterToken: response.Token});
	});
};

module.exports = exports = GoogleOauth;