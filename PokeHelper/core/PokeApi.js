/**
 * @providesModule PokeApi
 * Created by easy on 16/7/23.
 */

'use strict'

let Protobuf = require('react-native-protobuf');
let ZZNetwork = require('ZZNetwork');
let S2Geometry = require('S2Geometry');

let POGOProtosScheme = require('../resources/POGOProtos.json');
let POGOProtos = Protobuf.loadJson(POGOProtosScheme).build().POGOProtos;

var RequestEnvelope = POGOProtos.Networking.Envelopes.RequestEnvelope;
var ResponseEnvelope = POGOProtos.Networking.Envelopes.ResponseEnvelope;

var Requests = POGOProtos.Networking.Requests;
var Request = Requests.Request;
var Messages = POGOProtos.Networking.Requests.Messages;
var Responses = POGOProtos.Networking.Responses;

var apiUrl = 'https://pgorelease.nianticlabs.com/plfe/rpc';

let defaultPokeApi = undefined;

class PokeApi {

	static instance() {
		if (!defaultPokeApi) {
			defaultPokeApi = new PokeApi();
		}
		return defaultPokeApi;
	}

	constructor(props) {
		props = props || {};
		this.accessToken = props.accessToken || '';
		this.provider = props.provider || '';
		this.apiEndpoint = props.apiEndpoint || 'https://pgorelease.nianticlabs.com/plfe/352/rpc';
        this.location = {
            latitude : props.latitude || 0,
            longitude : props.longitude || 0
        };
        this.altitude = props.altitude || 0;
        this.hash = '05daf51635c82611d1aac95c0b051d3ec088a930';
		this.request.bind(this);
	}

	request(req,location,api_endpoint, access_token) {
		api_endpoint = api_endpoint || this.apiEndpoint;
		access_token = access_token || this.accessToken;
        location = location || this.location;

		return new Promise((resolve,reject)=> {
			var auth = new RequestEnvelope.AuthInfo({
				provider: this.provider,
				token: new RequestEnvelope.AuthInfo.JWT(access_token, 59)
			});// Auth
			var f_req = new RequestEnvelope({
				status_code: 2,
				request_id: 1469378659230941192,
				requests: req,
				latitude: location.latitude, longitude: location.longitude, altitude: this.altitude,
				auth_info: auth,
				unknown12: 989
			});

			//console.log(api_endpoint + ' request:');
			//console.log(f_req);

			var protobuf = f_req.encode().toBase64();

			var options = {
				url : api_endpoint,
				data : protobuf,
				'method' : 'POST', 'requestFormat' : 'DATA', 'responseFormat' : 'DATA',
				'headers' : {
					'User-Agent': 'Niantic App', 'Content-Type' : 'application/x-www-form-urlencoded',
					'Accept-Language':'zh-cn', 'Accept':'*/*', 'Accept-Encoding':'gzip, deflate',
					'Host':'pgorelease.nianticlabs.com', 'Connection':'keep-alive', 'Proxy-Connection':'keep-alive'
				}
			};

			ZZNetwork.request(options).then(response => {
				try {
					var f_ret = ResponseEnvelope.decode(response);
				} catch (e) {
					if (e.decoded) {
						// Truncated
						console.warn(e);
						f_ret = e.decoded; // Decoded message with missing required fields
					}
				}
				//console.log('response:');
				//console.log(f_ret);
				if (f_ret) {
					resolve(f_ret);
				} else {
					resolve(this.request(api_endpoint, access_token, req));
				}
			}, reject);
		});
	}

	async heartBeat(location) {
        location = location || this.location;
        let cid = (await S2Geometry.getMapObjectsCellId(location.latitude,location.longitude)).cell_id || 0;
        let time = Date.now();
        var getMapObjectReq = new Request(106,new Messages.GetMapObjectsMessage(cid,time,location.latitude,location.longitude).encode().toBuffer());
        var getHatchedEggsReq = new Request(126);//GET_HATCHED_EGGS
        var getInventoryReq = new Request(4,new Messages.GetInventoryMessage(time).encode().toBuffer());
        var checkAwardedBadgesReq = new Request(129);//CHECK_AWARDED_BADGES
        var downloadSettingReq = new Request(5,new Messages.DownloadSettingsMessage(this.hash).encode().toBuffer());
        let reqs = [
            getMapObjectReq, getHatchedEggsReq, getInventoryReq, checkAwardedBadgesReq, downloadSettingReq
        ];
        return this.request(reqs,location).then(f_ret => {
            let getMapObjectsResp = Responses.GetMapObjectsResponse.decode(f_ret.returns[0]);
            let getHatchedEggsResp = Responses.GetHatchedEggsResponse.decode(f_ret.returns[1]);
            let getInventoryResp = Responses.GetInventoryResponse.decode(f_ret.returns[2]);
            let checkAwardedBadgesResp = Responses.CheckAwardedBadgesResponse.decode(f_ret.returns[3]);
            let downloadSettingResp = Responses.DownloadSettingsResponse.decode(f_ret.returns[4]);
            return {
                map_objects:getMapObjectsResp,
                hatched_eggs:getHatchedEggsResp,
                inventory:getInventoryResp,
                awarded_badges:checkAwardedBadgesResp,
                download_setting:downloadSettingResp
            };
        });
    }

	getApiEndpoint() {//GET_PLAYER GET_HATCHED_EGGS GET_INVENTORY CHECK_AWARDED_BADGES DOWNLOAD_SETTINGS
		var req = [new Request(2), new Request(126), new Request(4), new Request(129), new Request(5)];
		return this.request(req,this.location, apiUrl, this.accessToken).then(f_ret => {
			var api_endpoint = 'https://' + f_ret.api_url + '/rpc';
			this.apiEndpoint = api_endpoint;
			return api_endpoint;
		});
	}

	//玩家更新
	playerUpdate(location) {//PLAYER_UPDATE
        location = location || this.location;
		var req = new Request(1,new Messages.PlayerUpdateMessage(location.latitude,location.longitude).encode().toBuffer());
		return this.request(req,location).then(f_ret => {
			return Responses.GetPlayerResponse.decode(f_ret.returns[0]);
		});
	}

	//获取玩家信息
	getPlayer() {//GET_PLAYER
		var req = new Request(2);
		return this.request(req).then(f_ret => {
			return Responses.GetPlayerResponse.decode(f_ret.returns[0]);
		});
	}

	//获取存货
	getInventory(last_timestamp_ms,item_been_seen) {//GET_INVENTORY
		var req = new Request(4,new Messages.GetInventoryMessage(last_timestamp_ms,item_been_seen).encode().toBuffer());
		return this.request(req).then(f_ret => {
			return Responses.GetInventoryResponse.decode(f_ret.returns[0]);
		});
	}

	//下载设置
	downloadSettings(hash) {//DOWNLOAD_SETTINGS
		var req = new Request(5,new Messages.DownloadSettingsMessage(hash).encode().toBuffer());
		return this.request(req).then(f_ret => {
			return Responses.DownloadSettingsResponse.decode(f_ret.returns[0]);
		});
	}

	//下载项目模板
	downloadItemTemplates(){//DOWNLOAD_ITEM_TEMPLATES
		var req = new Request(6);
		return this.request(req).then(f_ret => {
			return Responses.DownloadItemTemplatesResponse.decode(f_ret.returns[0]);
		});
	}

	//下载远程配置版本
	downloadRemoteConfigVersion(/*params see DownloadRemoteConfigVersionMessage*/){//DOWNLOAD_REMOTE_CONFIG_VERSION
		var req = new Request(7);
		return this.request(req).then(f_ret => {
			return Responses.DownloadRemoteConfigVersionResponse.decode(f_ret.returns[0]);
		});
	}

	//补给站搜索 //todo 未验证
	fortSearch(fort_id,fort_location,location) {//FORT_SEARCH
        location = location || this.location;
		var req = new Request(101,new Messages.FortSearchMessage(fort_id,location.latitude,location.longitude,fort_location.latitude,fort_location.longitude).encode().toBuffer());
		return this.request(req,location).then(f_ret => {
			return Responses.FortSearchResponse.decode(f_ret.returns[0]);
		});
	}

	//遭遇 //todo 未验证
	encounter(encounter_id,spawn_point_id,location) {//ENCOUNTER
        location = location || this.location;

		var req = new Request(102,new Messages.EncounterMessage(encounter_id,spawn_point_id,location.latitude,location.longitude).encode().toBuffer());
		return this.request(req,location).then(f_ret => {
			return Responses.EncounterResponse.decode(f_ret.returns[0]);
		});
	}

	//捕获精灵 //todo 未验证
	catchPokemon(encounter_id,pokeball,normalized_reticle_size,spawn_point_id,hit_pokemon,spin_modifier,normalized_hit_position) {//CATCH_POKEMON
		var req = new Request(103,new Messages.EncounterMessage(encounter_id,pokeball,normalized_reticle_size,spawn_point_id,hit_pokemon,spin_modifier,normalized_hit_position).encode().toBuffer());
		return this.request(req).then(f_ret => {
			return Responses.CatchPokemonResponse.decode(f_ret.returns[0]);
		});
	}

	//补给站详情 //todo 未验证
	fortDetails(fort_id,location) {//FORT_DETAILS
        location = location || this.location;
		var req = new Request(104,new Messages.FortDetailsMessage(fort_id,location.latitude,location.longitude).encode().toBuffer());
		return this.request(req,location).then(f_ret => {
			return Responses.FortDetailsResponse.decode(f_ret.returns[0]);
		});
	}

	//项目使用
	itemUse() {//ITEM_USE
		//unuse
	}

	async getMapObjects(since_timestamp_ms,location,cell_id) {//GET_MAP_OBJECTS
        location = location || this.location;
		let cid = (await S2Geometry.getMapObjectsCellId(location.latitude,location.longitude)).cell_id || 0;
		cell_id =  cell_id || cid;
		var req = new Request(106,new Messages.GetMapObjectsMessage(cell_id,since_timestamp_ms,location.latitude,location.longitude).encode().toBuffer());
		return this.request(req,location).then(f_ret => {
			return Responses.GetMapObjectsResponse.decode(f_ret.returns[0]);
		});
	}
}


module.exports = PokeApi;