/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"

#import "RCTBundleURLProvider.h"
#import "RCTRootView.h"

#import <AFNetworking/AFNetworking.h>


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSURL *jsCodeLocation;

  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];

  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"PokeHelper"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  
  
  
 // http://www.serebii.net/pokemongo/pokemon/001.png
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  manager.requestSerializer = [AFHTTPRequestSerializer serializer];
  manager.responseSerializer = [AFHTTPResponseSerializer serializer];
  
  for (int i = 1;i <= 151;i++){
    NSString *url = [[NSString alloc] initWithFormat:@"http://www.serebii.net/pokemongo/pokemon/%03d.png",i];
    
    [manager GET:url parameters:@{}  progress:^(NSProgress *  downloadProgress) {
      
    } success:^(NSURLSessionDataTask *  task, NSData*   responseObject) {
      NSString *path = [[NSString alloc] initWithFormat:@"/Users/easy/Desktop/pokemons/%d.png",i];
      [responseObject writeToFile:path atomically:YES];
    } failure:^(NSURLSessionDataTask *  task, NSError *  error) {
      NSLog(@"2");
    }];
  }
  
  
  [self.window makeKeyAndVisible];
  return YES;
}

@end
