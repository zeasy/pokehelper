//
//  S1Angle.h
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface S1Angle : NSObject
@property (nonatomic, assign) double radians;


+(instancetype) radians:(double) radians;
+(instancetype) degrees:(double) degrees;
@end
