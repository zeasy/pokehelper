//
//  S2CellId.m
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "S2CellId.h"
#import "S2.h"
#define FACE_BITS  3
#define NUM_FACES 6
#define MAX_LEVEL 30
#define POS_BITS (2 * MAX_LEVEL + 1)
#define MAX_SIZE (1 << MAX_LEVEL)

#define SWAP_MASK 0x01
#define INVERT_MASK 0x02
#define LOOKUP_BITS 4


UInt16 LOOKUP_POS[1 << (2 * LOOKUP_BITS + 2)] = {0};
UInt16 LOOKUP_IJ[1 << (2 * LOOKUP_BITS + 2)] = {0};

void initLookupCell(int level, int i, int j,
               int origOrientation, int pos, int orientation) {
  if (level == LOOKUP_BITS) {
    int ij = (i << LOOKUP_BITS) + j;
    LOOKUP_POS[(ij << 2) + origOrientation] = (pos << 2) + orientation;
    LOOKUP_IJ[(pos << 2) + origOrientation] = (ij << 2) + orientation;
  } else {
    level++;
    i <<= 1;
    j <<= 1;
    pos <<= 2;
    // Initialize each sub-cell recursively.
    for (int subPos = 0; subPos < 4; subPos++) {
      int ij = [S2 posToIJ:orientation p:subPos];
      int orientationMask = [S2 posToOrientation:subPos];
      initLookupCell(level, i + (ij >> 1), j + (ij & 1), origOrientation,
                     pos + subPos, orientation ^ orientationMask);
    }
  }
}

@implementation S2CellId

+(void)initialize {
  if ([self class] == [S2CellId class]) {
    initLookupCell(0, 0, 0, 0, 0, 0);
    initLookupCell(0, 0, 0, SWAP_MASK, 0, SWAP_MASK);
    initLookupCell(0, 0, 0, INVERT_MASK, 0, INVERT_MASK);
    initLookupCell(0, 0, 0, SWAP_MASK | INVERT_MASK, 0, SWAP_MASK | INVERT_MASK);
  }
}

+(long) lowestOnBitForLevel:(UInt64) level {
  return 1L << (2 * (MAX_LEVEL - MIN(level, MAX_LEVEL)));
}

-(S2CellId *) parent:(UInt64) level {
  // assert (isValid() && level >= 0 && level <= this.level());
  long newLsb = [[self class] lowestOnBitForLevel:level];
  S2CellId *cellId = [[S2CellId alloc] init];
  cellId._id = (self._id & -newLsb) | newLsb;
  return cellId;
}

+(instancetype) fromLatLng:(S2LatLng *) ll {
  return [self fromPoint:[ll toPoint]];
}

+(instancetype) fromPoint:(S2Point *) pt {
  int face = [S2Projections xyzToFace:pt];
  R2Vector *uv = [S2Projections validFaceXyzToUv:face point:pt];
  int i = [self stToIJ:[S2Projections uvToST:uv.x]];
  int j = [self stToIJ:[S2Projections uvToST:uv.y]];
  return [self fromFaceIJ:face i:i j:j];
}

+(int) stToIJ:(double) s {
  int m = MAX_SIZE / 2; // scaling multiplier
  return (int) MAX(0, MIN(2 * m - 1, round(m * s + (m - 0.5))));
}


+(instancetype) fromFaceIJ:(int) face i:(int) i j:(int) j {

  UInt32 n[2] = {0, face << (POS_BITS - 33)};
  
  int bits = (face & SWAP_MASK);
  
  for (int k = 7; k >= 0; --k) {
    bits = [self getBits:n i:i j:j k:k bits:bits];
  }
  S2CellId *s = [[S2CellId alloc] init];
  s._id = (((((UInt64)n[1]) << 32) + n[0]) << 1) + 1;
  return s;
}

+(int) getBits:(UInt32[]) n i:(int) i j:(int) j k:(int) k bits:(int) bits {
  int mask = (1 << LOOKUP_BITS) - 1;
  bits += (((i >> (k * LOOKUP_BITS)) & mask) << (LOOKUP_BITS + 2));
  bits += (((j >> (k * LOOKUP_BITS)) & mask) << 2);
  bits = LOOKUP_POS[bits];
  n[k >> 2] |= ((((long) bits) >> 2) << ((k & 3) * 2 * LOOKUP_BITS));
  bits &= (SWAP_MASK | INVERT_MASK);
  return bits;
}
@end
