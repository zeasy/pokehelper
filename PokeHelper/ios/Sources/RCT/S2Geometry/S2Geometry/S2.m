//
//  S2.m
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "S2.h"
#define SWAP_MASK  0x01
#define INVERT_MASK 0x02
int POS_TO_IJ[4][4] = {
  // 0 1 2 3
  {0, 1, 3, 2}, // canonical order: (0,0), (0,1), (1,1), (1,0)
  {0, 2, 3, 1}, // axes swapped: (0,0), (1,0), (1,1), (0,1)
  {3, 2, 0, 1}, // bits inverted: (1,1), (1,0), (0,0), (0,1)
  {3, 1, 0, 2}, // swapped & inverted: (1,1), (0,1), (0,0), (1,0)
};

int POS_TO_ORIENTATION[4] = {SWAP_MASK, 0, 0, INVERT_MASK + SWAP_MASK};


@implementation S2
+(int) posToIJ:(int) orientation p:(int) position {
  return POS_TO_IJ[orientation][position];
}

+(int) posToOrientation:(int) position {
  return POS_TO_ORIENTATION[position];
}
@end
