//
//  R2Vector.h
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface R2Vector : NSObject

@property (nonatomic, assign) double x;
@property (nonatomic, assign) double y;

@end
