//
//  S2.h
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface S2 : NSObject
+(int) posToIJ:(int) orientation p:(int) position;
+(int) posToOrientation:(int) position;
@end
