//
//  S1Angle.m
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "S1Angle.h"

@implementation S1Angle


+(instancetype) radians:(double) radians {
  S1Angle *angle = [[S1Angle alloc] init];
  angle.radians = radians;
  return angle;
}

+(instancetype) degrees:(double) degrees {
  S1Angle *angle = [[S1Angle alloc] init];
  angle.radians = degrees * (M_PI / 180);
  return angle;
}

@end
