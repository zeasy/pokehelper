//
//  S2Projections.m
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "S2Projections.h"

enum Projections {
  S2_LINEAR_PROJECTION, S2_TAN_PROJECTION, S2_QUADRATIC_PROJECTION
};

int S2_PROJECTION = S2_QUADRATIC_PROJECTION;

@implementation S2Projections

+(int) xyzToFace:(S2Point *) pt {
  int face = [pt largestAbsComponent];
  if ([pt get:face] < 0) {
    face += 3;
  }
  return face;
}

+(R2Vector *) validFaceXyzToUv:(int) face point:(S2Point *) p {
  double pu;
  double pv;
  switch (face) {
      case 0:
      pu = p.y / p.x;
      pv = p.z / p.x;
      break;
      case 1:
      pu = -p.x / p.y;
      pv = p.z / p.y;
      break;
      case 2:
      pu = -p.x / p.z;
      pv = -p.y / p.z;
      break;
      case 3:
      pu = p.z / p.x;
      pv = p.y / p.x;
      break;
      case 4:
      pu = p.z / p.y;
      pv = -p.x / p.y;
      break;
    default:
      pu = -p.y / p.z;
      pv = -p.x / p.z;
      break;
  }
  R2Vector *v = [[R2Vector alloc] init];
  v.x = pu;
  v.y = pv;
  return v;
}

+(double) uvToST:(double) u {
  switch (S2_PROJECTION) {
      case S2_LINEAR_PROJECTION:
      return u;
      case S2_TAN_PROJECTION:
      return (4 * M_1_PI) * atan(u);
      case S2_QUADRATIC_PROJECTION:
      if (u >= 0) {
        return sqrt(1 + 3 * u) - 1;
      } else {
        return 1 - sqrt(1 - 3 * u);
      }
    default:
      return 0;
  }
}

@end
