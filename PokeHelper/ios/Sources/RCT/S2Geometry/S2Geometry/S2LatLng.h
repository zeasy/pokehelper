//
//  S2LatLng.h
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "S2Point.h"
#import "S1Angle.h"

@interface S2LatLng : NSObject

@property (nonatomic, assign) double latRadians;
@property (nonatomic, assign) double lngRadians;

-(S1Angle *) lat;
-(S1Angle *) lng;
-(S2Point *) toPoint;

+(instancetype) fromDegrees:(double) latDegrees lng:(double) lngDegrees;

@end
