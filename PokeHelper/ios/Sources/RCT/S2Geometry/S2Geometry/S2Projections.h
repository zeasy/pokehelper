//
//  S2Projections.h
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "S2Point.h"
#import "R2Vector.h"

@interface S2Projections : NSObject


+(int) xyzToFace:(S2Point *) pt;
+(R2Vector *) validFaceXyzToUv:(int) face point:(S2Point *) pt;
+(double) uvToST:(double) u;

@end
