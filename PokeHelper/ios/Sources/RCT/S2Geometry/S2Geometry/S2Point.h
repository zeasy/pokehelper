//
//  S2Point.h
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface S2Point : NSObject
@property (nonatomic, assign) double x;
@property (nonatomic, assign) double  y;
@property (nonatomic, assign) double  z;

-(double) get:(int) axis;
-(int) largestAbsComponent;
@end
