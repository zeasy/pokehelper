//
//  S2CellId.h
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "S2LatLng.h"
#import "S2Point.h"
#import "S2Projections.h"
#import "R2Vector.h"

@interface S2CellId : NSObject
@property (nonatomic, assign) UInt64 _id;

+(instancetype) fromLatLng:(S2LatLng *) ll;
-(S2CellId *) parent:(UInt64) level;
@end
