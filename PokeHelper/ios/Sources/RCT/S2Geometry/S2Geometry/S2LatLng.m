//
//  S2LatLng.m
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "S2LatLng.h"
#import "S1Angle.h"

@implementation S2LatLng

-(double) latDegrees {
  return 180.0 / M_PI * self.latRadians;
}

-(double) lngDegrees {
  return 180.0 / M_PI * self.lngRadians;
}

-(S1Angle *) lat {
  return [S1Angle radians:self.latRadians];
}

-(S1Angle *) lng {
  return [S1Angle radians:self.lngRadians];
}

-(S2Point *) toPoint {
  double phi = [self lat].radians;
  double theta = [self lng].radians;
  double cosphi = cos(phi);
  S2Point *pt = [[S2Point alloc] init];
  pt.x = cos(theta) * cosphi;
  pt.y = sin(theta) * cosphi;
  pt.z = sin(phi);
  return pt;
}

+(instancetype) fromDegrees:(double) latDegrees lng:(double) lngDegrees {
  S1Angle *lat = [S1Angle degrees:latDegrees];
  S1Angle *lng = [S1Angle degrees:lngDegrees];

  S2LatLng *latLng = [[S2LatLng alloc] init];
  latLng.latRadians = [lat radians];
  latLng.lngRadians = [lng radians];
  return latLng;
}

@end
