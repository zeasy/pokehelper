//
//  S2Point.m
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "S2Point.h"

@implementation S2Point

-(double) get:(int) axis {
  return (axis == 0) ? self.x : (axis == 1) ? self.y : self.z;
}

-(int) largestAbsComponent {
  S2Point *temp = [[self class] fabs:self];
  if (temp.x > temp.y) {
    if (temp.x > temp.z) {
      return 0;
    } else {
      return 2;
    }
  } else {
    if (temp.y > temp.z) {
      return 1;
    } else {
      return 2;
    }
  }
}

+(S2Point *) fabs:(S2Point *) p {
  S2Point *pt = [[S2Point alloc] init];
  pt.x = fabs(p.x);
  pt.y = fabs(p.y);
  pt.z = fabs(p.z);
  return pt;
}

@end
