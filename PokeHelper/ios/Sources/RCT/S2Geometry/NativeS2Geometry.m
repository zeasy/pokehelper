//
//  NativeS2Geometry.m
//  PokeHelper
//
//  Created by EASY on 16/7/24.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "NativeS2Geometry.h"
#import <RCTConvert.h>
#import "S2CellId.h"
#import "S2LatLng.h"

@implementation NativeS2Geometry
RCT_EXPORT_MODULE()


RCT_EXPORT_METHOD(getCellId:(NSDictionary *) options r:(RCTPromiseResolveBlock) resolve rej:(RCTPromiseRejectBlock) reject) {
  options = [RCTConvert NSDictionary:options];
  double latitude = [RCTConvert double:options[@"latitude"]];
  double longitude = [RCTConvert double:options[@"longitude"]];
  
  S2LatLng *latLng = [S2LatLng fromDegrees:latitude lng:longitude];

  
  S2CellId *cellId = [S2CellId fromLatLng:latLng];
  
  if (options[@"level"]) {
    cellId = [cellId parent:[RCTConvert uint64_t:options[@"level"]]];
  }
  
  UInt64 cid = cellId._id;
  resolve(@{@"cell_id":[NSString stringWithFormat:@"%llu",cid]});
}

@end
