//
//  AFHTTPRawDataRequestSerializer.m
//  PokeHelper
//
//  Created by EASY on 16/7/23.
//  Copyright © 2016年 Facebook. All rights reserved.
//

#import "AFHTTPRawDataRequestSerializer.h"

@implementation AFHTTPRawDataRequestSerializer
- (NSURLRequest *)requestBySerializingRequest:(NSURLRequest *)request
                               withParameters:(id)parameters
                                        error:(NSError *__autoreleasing *)error {
  NSMutableURLRequest *mutableRequest = [[super requestBySerializingRequest:request withParameters:parameters error:error] mutableCopy];
  if ([parameters isKindOfClass:[NSData class]]) {
    [mutableRequest setHTTPBody:parameters];
    if (![mutableRequest valueForHTTPHeaderField:@"Content-Type"]) {
      [mutableRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    }

  }
    return mutableRequest;
}


@end
