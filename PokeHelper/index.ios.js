/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	View,
	AsyncStorage,
	TouchableHighlight,
    Navigator,
    NavigatorIOS
} from 'react-native';


class PokeHelper extends Component {

	constructor(props,context) {
		super(props,context);

        //let mons1 = require('./resources/pokemons.json').pokemon;
        //let mons2 = require('./resources/pokemons2.json');
        //
        //let mons = [];
        //let pokemonsJs = 'module.exports = {';
        //
        //for(let i = 0; i < mons2.length;i++) {
        //    let m1 = mons1[i];
        //    let m2 = mons2[i];
        //    m1 = {...m1,...m2};
        //    mons.push(m1);
        //    pokemonsJs += '"' + (i+1) +'":()=>require("./pokemons/'+(i+1)+'.png"),\n';
        //}
        //pokemonsJs+='}';
        //console.log(JSON.stringify(mons));
        //console.log(pokemonsJs);
	}

	render() {
		return (
			<NavigatorIOS initialRoute={{component:require('./RootScreen'),title:'Root'}} style={{flex:1}}/>
		);
	}
}


AppRegistry.registerComponent('PokeHelper', () => PokeHelper);
