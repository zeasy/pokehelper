/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
let user = 'zeasy@qq.com';
let pass = 'Zhuyi520';
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    AsyncStorage,
    TouchableHighlight
} from 'react-native';


let SVProgressHUD = require('SVProgressHUD');
let PokeApi = require('PokeApi');
let PokeIO = require('PokeIO');
class RootScreen extends Component {

    constructor(props,context) {
        super(props,context);


        this.pokeApi = PokeApi.instance();

        this.pokeApi.provider = 'google';
        this.pokeApi.location.latitude = 37.787994;
        this.pokeApi.location.longitude = -122.407440;







        (async ()=>{
            SVProgressHUD.showWithMaskType()
            let accessToken = await AsyncStorage.getItem('accessToken',accessToken);
            if (accessToken) {
                this.pokeApi.accessToken = accessToken;
                this.pokeApi.getApiEndpoint().then(result => {
                    console.log(result);
                    SVProgressHUD.dismiss();
                }).catch(err => SVProgressHUD.dismiss());

            } else {
                PokeIO.GetAccessToken(user,pass,(err, accessToken) => {
                    this.pokeApi.accessToken = accessToken;

                    AsyncStorage.setItem('accessToken',accessToken);

                    this.pokeApi.getApiEndpoint().then(result => {
                        console.log(result);
                        SVProgressHUD.dismiss();

                    }).catch(err => {
                        console.log(err);
                        SVProgressHUD.dismiss();
                    });
                });
            }
        })();


    }
    onPress() {


        this.pokeApi.getMapObjects().then(ret => {
            let cells = ret.map_cells[0].nearby_pokemons;
        });
    }

    onMapPress() {
        this.props.navigator.push({
            component : require('./NearbyScreen')
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Welcome to React Native!
                </Text>
                <Text style={styles.instructions}>
                    To get started, edit index.ios.js
                </Text>
                <Text style={styles.instructions}>
                    Press Cmd+R to reload,{'\n'}
                    Cmd+D or shake for dev menu
                </Text>
                <TouchableHighlight onPress={this.onPress.bind(this)}>
                    <Text style={styles.instructions}>Request</Text>
                </TouchableHighlight>
                <TouchableHighlight onPress={this.onMapPress.bind(this)}>
                    <Text style={styles.instructions}>Map</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

module.exports = RootScreen;
