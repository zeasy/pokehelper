/**
 * Created by easy on 16/7/25.
 */

'use strict'

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    AsyncStorage,
    TouchableHighlight
} from 'react-native';

let PokeApi = require('PokeApi');
let MapView = require('react-native-maps');
let Pokemons = require('PokeImages');
let SVProgressHUD = require('SVProgressHUD');


class NearbyScreen extends Component {

    constructor(props, context) {
        super(props,context);
        this.pokeApi = PokeApi.instance();
        this.pokemons = require('./resources/pokemons.json').pokemon;

        this.state = {
            pokemons : [],
            points : [],
            locations : [],
            location : this.pokeApi.location
        }

        this.enterPoints = [];
    }

    componentDidMount() {
        this.requestMons(this.state.location);
    }

    _requestMons(location) {


        return this.pokeApi.getMapObjects(Date.now(),location).then(ret => ret.map_cells[0]);

        //return this.pokeApi.heartBeat(location).then(ret => {
        //    return ret.map_objects.map_cells[0];
        //});
    }

    requestMons(location) {

        let center = location;
        let locs = [];
        let reqs = [];
        for (let i = -1;i < 2;i++) {
            for (let j = -1;j < 2;j++){
                let loc = {latitude:i * 0.002 + center.latitude,longitude:j * 0.002 + center.longitude};
                locs.push(loc);
                reqs.push(this._requestMons(loc));
            }
        }

        SVProgressHUD.showWithMaskType();
        Promise.all(reqs).then(cells => {
            console.log(cells);
            SVProgressHUD.dismiss();

            let pokemons = {};
            let points = {};

            cells.forEach(cell => {
                //pokemons = pokemons.concat(cell.catchable_pokemons);
                cell.catchable_pokemons.forEach(mon => {
                    pokemons[mon.latitude+','+mon.longitude] = mon;
                });
                cell.spawn_points.forEach(point => {
                    points[point.latitude+','+point.longitude] =point;
                });
                //points = points.concat(cell.spawn_points);
            });
            let pms = Object.keys(pokemons).forEach(t => {
                return pokemons[t];
            });

            let pts = Object.keys(points).forEach(t => {
                return points[t];
            });

            this.setState({locations:locs,pokemons:pms,points:pts});

        }).catch(err => {
            console.log(err);
            SVProgressHUD.dismiss();
        });



    }

    onPress(event) {
        let coordinate = event.nativeEvent.coordinate;//this.state.location;//
        this.requestMons(coordinate.latitude,coordinate.longitude);
        this.setState({location:coordinate});
    }

    render() {
        console.log('render');
        return (
            <MapView
                initialRegion={{...this.state.location,latitudeDelta: 0.005,longitudeDelta: 0.005,
    }} style={styles.container} region={{...this.state.location,latitudeDelta: 0.005,longitudeDelta: 0.005,
    }}>
                {
                    this.state.points && this.state.points.map(point => {
                        return (<MapView.Marker
                            coordinate={point}
                            key={point.latitude +'.'+point.longitude}
                            image={require('./resources/images/dotBlue.png')}
                        />);
                    })
                }
                {
                    this.state.pokemons && this.state.pokemons.map(mon => {
                        let id = parseInt(mon.pokemon_id || mon.pokemon_data.pokemon_id);
                        let image = Pokemons.getImage(id);
                        let poke = this.pokemons[id - 1];
                        console.log(mon);
                        return (
                            <MapView.Marker
                                coordinate={{latitude: mon.latitude,longitude: mon.longitude}}
                                title={poke.cn_name}
                                key={Math.random()}
                                image={image}
                            />);
                    })
                }
                {
                    this.state.locations && this.state.locations.map(loc => {
                        return (
                            (<MapView.Marker
                                coordinate={loc}
                                image={require('./resources/images/pokeball.png')}
                                key={loc.latitude +':'+loc.longitude}
                            />)
                        );
                    })
                }

            </MapView>

        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});


module.exports = NearbyScreen;